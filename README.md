# Bubbles Android SDK #

Bubbles Android SDK gives you the ability to provide your customers with great In-App customer care services. To have trial access, kindly submit your access request through [Bubbles.cc](http://bubbles.cc/) website.

This repo shows an example of how you can integrate the service with your great Android mobile applications.

## Installation ##
-----------

* In order to install the SDK follow the below steps:

**Eclipse:**

1. Right click on the on your android project  and select properties 
1. Select from tabs  Java Build Path -> libraries  -> Add External JARs
1. Browse to select the bubbleschatsdk.jar file 

**Android Studio:**

1. Add new folder called libs.
1. Copy the jar file in libs folder.
1. Open build.gradle file to compile the jar.
1. Write in dependencies section 	
```
#!java

compile files ('libs/filename.jar')
```

## Getting Started ##
-----------
First, you will need an API key. 

* Once you got your server access. Generate your SDK key from the "Settings" tab in your Bubbles server. 
* Note down your API key because you will need it in your code.
* Install the SDK as mentioned above.
* To start using the SDK, first you need to tell it your server name and your API Key. You do this by calling the following method:


```
#!java

ConfigManager.setServerAppKey(SERVER_SDK_KEY);
ConfigManager.setServerName(SERVER_NAME);
```


### Starting the chat ###
-----------

* The whole chat functionality is managed through the singleton class called "ChatManager".
* First you will need to specify its Listeners:


```
#!java

chatManager.setSessionListener(this)
chatManager.setCacheManager(this)
```


Then call the:


```
#!java

chatManager.enterChatWithCustomerName(customerName,customerId, userLocation,deviceToken)
```


All the parameters of this method are optional except the first one.
After calling this method you will start to receive your listener callbacks, and you can re-act to them as desired.



### API References ###
-----------

* For in-depth details for each method, parameter, class or interface please refer to the accompanied docs.


## Apps using Bubbles SDK ##
-----------

* [Bubbles Care](https://itunes.apple.com/us/app/bubbles.cc/id922468338)
* [Ketwket](https://itunes.apple.com/us/app/kyt-wkyt-ketwket/id819371940)
* [Infinity Advertising Care](https://play.google.com/store/apps/details?id=cc.bubbles.Infinity)