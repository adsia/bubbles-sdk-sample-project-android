package com.adsia.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.adsia.dialogs.DepartmentsDialog;
import com.adsia.exampleApp.R;
import com.adsia.models.Department;

public class DepartmentsAdapter extends BaseAdapter {
	private DepartmentsDialog departmentsDialog;
	private List<Department> departments;
	private LayoutInflater inflater;

	public DepartmentsAdapter(DepartmentsDialog departmentsDialog,
			List<Department> departments) {
		this.departmentsDialog = departmentsDialog;
		this.departments = departments;
		inflater = (LayoutInflater) departmentsDialog.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {

		if (departments != null) {
			return departments.size();
		} else {
			return 0;
		}

	}

	@Override
	public Department getItem(int position) {

		return departments.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		RadioButton radioButton = (RadioButton) inflater.inflate(
				R.layout.department_list_item, null);

		String name = departments.get(position).getName();
		radioButton.setText(name);
		radioButton.setTag(position);
		radioButton.setOnCheckedChangeListener(departmentsDialog);
		return radioButton;
	}

}
