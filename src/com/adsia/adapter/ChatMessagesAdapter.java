package com.adsia.adapter;

import java.util.ArrayList;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adsia.exampleApp.R;
import com.adsia.models.ChatMessage;

public class ChatMessagesAdapter extends BaseAdapter {
	private ArrayList<ChatMessage> chatMessages = null;
	private Context context = null;
	private LayoutInflater inflater = null;
	private Bitmap operatorAvatar = null;

	public ChatMessagesAdapter(Context context,
			ArrayList<ChatMessage> chatMessages) {
		this.context = context;
		this.chatMessages = chatMessages;
		inflater = LayoutInflater.from(context);

	}

	@Override
	public int getCount() {
		return chatMessages.size();
	}

	@Override
	public ChatMessage getItem(int position) {
		return chatMessages.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ChatMessage chatMessage = chatMessages.get(position);

		// check if message log

		if (chatMessage == null || chatMessage.getWho() == null) {
			view = getMessageErrorView(chatMessage, position, view, parent);
		} else if (chatMessage.getWho().equals(
				ChatMessage.FAILED_TO_BE_DELIVERED)) {
			view = getMessageNotDileveredView(chatMessage, position, view,
					parent);
		} else if (chatMessage.getWho().equals(ChatMessage.CUSTOMER)) {
			view = getCustomerRowView(chatMessage, position, view, parent);

		} else if (chatMessage.getWho().equals(ChatMessage.OPERATOR)) {
			view = getOperatorRowView(chatMessage, position, view, parent);
		} else if (chatMessage.getWho().equals(ChatMessage.SYSTEM_ERROR)) {
			view = getSystemErrorRowView(chatMessage, position, view, parent);
		}

		ViewHolder holder = (ViewHolder) view.getTag();

		return view;

	}

	private View getMessageErrorView(ChatMessage chatMessage, int position,
			View view, ViewGroup parent) {
		if (view == null) {
			view = new View(context);
		}
		view.setVisibility(View.GONE);
		return view;
	}

	private View getMessageNotDileveredView(ChatMessage chatMessage,
			int position, View view, ViewGroup parent) {
		if (view == null) {
			view = inflater.inflate(
					R.layout.chat_user_message_item_not_delivered, parent,
					false);
		}
		ViewHolder holder = (ViewHolder) view.getTag();
		if (holder == null) {
			holder = new ViewHolder();
			holder.tvMessageText = (TextView) view
					.findViewById(R.id.tvMessageTextMessageNotDelivered);
			view.setTag(holder);
		}

		holder.tvMessageText.setText(chatMessage.getMsg());

		return view;
	}

	private View getSystemErrorRowView(ChatMessage chatMessage, int position,
			View view, ViewGroup parent) {
		if (view == null) {
			view = inflater
					.inflate(R.layout.system_message_item, parent, false);
		}
		ViewHolder holder = (ViewHolder) view.getTag();
		if (holder == null) {
			holder = new ViewHolder();
			holder.tvMessageText = (TextView) view
					.findViewById(R.id.tvSystemMessageText);
			view.setTag(holder);
		}

		holder.tvMessageText.setText(chatMessage.getMsg());

		return view;
	}

	private View getOperatorRowView(ChatMessage chatMessage, int position,
			View view, ViewGroup parent) {
		return getNormalMessageRowView(chatMessage, position, view, parent,
				R.layout.chat_user_message_item_right);
	}

	private View getCustomerRowView(ChatMessage chatMessage, int position,
			View view, ViewGroup parent) {
		return getNormalMessageRowView(chatMessage, position, view, parent,
				R.layout.chat_user_message_item_left);
	}

	private View getNormalMessageRowView(ChatMessage chatMessage, int position,
			View view, ViewGroup parent, int layoutId) {
		if (view == null) {
			view = inflater.inflate(layoutId, parent, false);
		}

		ViewHolder holder = (ViewHolder) view.getTag();
		if (holder == null) {
			holder = new ViewHolder();

			holder.tvMessageText = (TextView) view
					.findViewById(R.id.tvMessageText);
			holder.tvMessageDate = (TextView) view
					.findViewById(R.id.tvMessageDate);
			view.setTag(holder);
		}

		// check if text is map

		checkMsgContainMapsData(chatMessage, holder);

		checkMsgContainEmailData(chatMessage, holder);

		return view;
	}

	private void checkMsgContainEmailData(final ChatMessage chatMessage,
			final ViewHolder holder) {

		// "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"
		String emailRegx = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

		if (Pattern.matches(emailRegx, chatMessage.getMsg())) {
			final SpannableString ss = new SpannableString(chatMessage.getMsg());

			ClickableSpan clickableSpan = new ClickableSpan() {
				@Override
				public void onClick(View textView) {

					try {
						String emailString = chatMessage.getMsg();

						Intent emailIntent = new Intent(Intent.ACTION_SEND);
						emailIntent.setData(Uri.parse("mailto:"));
						emailIntent.setType("text/plain");
						emailIntent.putExtra(Intent.EXTRA_EMAIL,
								new String[] { emailString });
						context.startActivity(emailIntent);
						Log.i("TAG", emailString);
					} catch (Exception e) {
						if (e != null && e.getMessage() != null)
							Log.i("TAG", e.getMessage());

					}
				}
			};
			ss.setSpan(clickableSpan, 0, chatMessage.getMsg().length(),
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

			holder.tvMessageText.setText(ss);
			holder.tvMessageText.setMovementMethod(LinkMovementMethod
					.getInstance());

		}

	}

	private void checkMsgContainMapsData(final ChatMessage chatMessage,
			ViewHolder holder) {

		final boolean iswikimapia = chatMessage.getMsg().contains("wikimapia");
		final boolean isGoolgeMap = chatMessage.getMsg().contains("maps");
		if (iswikimapia || isGoolgeMap) {

			final SpannableString ss = new SpannableString(chatMessage.getMsg());

			ClickableSpan clickableSpan = new ClickableSpan() {
				@Override
				public void onClick(View textView) {

					try {
						String mapString = chatMessage.getMsg();

						if (isGoolgeMap) {
							Intent intent = new Intent(
									android.content.Intent.ACTION_VIEW,
									Uri.parse(mapString));
							context.startActivity(intent);
						} else {

							String[] split = mapString.split("&");
							String lat = split[1].split("=")[1];
							String lng = split[2].split("=")[1];

							String geoUri = "http://maps.google.com/maps?q=loc:"
									+ lat + "," + lng;

							Uri uri = Uri.parse(geoUri);
							Intent intent = new Intent(
									android.content.Intent.ACTION_VIEW, uri);
							context.startActivity(intent);

							Log.i("TAG", "lat : " + lat + " lng : " + lng);
						}
					} catch (Exception e) {
						if (e != null && e.getMessage() != null)
							Log.i("TAG", e.getMessage());

					}
				}
			};
			ss.setSpan(clickableSpan, 0, chatMessage.getMsg().length(),
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

			holder.tvMessageText.setText(ss);
			holder.tvMessageText.setMovementMethod(LinkMovementMethod
					.getInstance());

		} else

			holder.tvMessageText.setText(chatMessage.getMsg());
	}

	@Override
	public int getItemViewType(int position) {
		try {
			ChatMessage chatMessage = chatMessages.get(position);

			String who = chatMessage.getWho();
			if (who.equals(ChatMessage.CUSTOMER)) {
				return 0;
			} else if (who.equals(ChatMessage.OPERATOR)) {
				return 1;
			} else if (who.equals(ChatMessage.FAILED_TO_BE_DELIVERED)) {
				return 2;
			} else {
				return 3;
			}
		} catch (Exception e) {
			if (e != null && e.getMessage() != null) {
				Log.e("adsia", e.getMessage());
				return 3;
			}

		}
		return 3;
	}

	@Override
	public int getViewTypeCount() {
		return 4;
	}

	// private View showLogsFeeback()
	// {
	// View feedbackLayout = inflater.inflate(R.layout.log_feedback_layout,
	// null);
	// rbLogMyRating = (RatingBar) feedbackLayout
	// .findViewById(R.id.rbConversationLogRate);
	// etLogMyComment = (EditText) feedbackLayout
	// .findViewById(R.id.etConversationLogComment);
	// disableRateBar(rbLogMyRating);
	// // showRateAndCommentValues();
	// return feedbackLayout;
	// }
	//
	// private void showRateAndCommentValues()
	// {
	// if (rate != null && !rate.equals("")) {
	// rbLogMyRating.setVisibility(View.VISIBLE);
	// rbLogMyRating.setRating(Float.valueOf(rate));
	// } else {
	// // rbLogMyRating.setVisibility(View.GONE);
	// }
	// if (comment != null && !comment.equals("")) {
	// etLogMyComment.setVisibility(View.VISIBLE);
	// etLogMyComment.setText(Farsi.Convert(comment));
	// } else {
	// etLogMyComment.setVisibility(View.GONE);
	// }
	// }
	//
	// private void disableRateBar(RatingBar rbLogMyRating)
	// {
	// rbLogMyRating.setOnTouchListener(new OnTouchListener() {
	// @Override
	// public boolean onTouch(View v, MotionEvent event)
	// {
	// return true;
	// }
	// });
	// }

	@SuppressWarnings("unused")
	private void preScaleAvatar() {
		Matrix m = new Matrix();
		m.preScale(-1, 1);
		operatorAvatar = Bitmap
				.createBitmap(operatorAvatar, 0, 0, operatorAvatar.getWidth(),
						operatorAvatar.getHeight(), m, false);
	}

	// private void getImageFromServer(final ImageView imageView,
	// final String urlString)
	// {
	//
	// Picasso.with(context).load(urlString).into(imageView);
	// }
	//
	// private LinearLayout systemMessageSenderCase(ChatMessage chatMessage)
	// {
	// LinearLayout systemMessageItemMainLayout = (LinearLayout) inflater
	// .inflate(R.layout.system_message_item, null);
	// TextView tvMessageText = (TextView) systemMessageItemMainLayout
	// .findViewById(R.id.tvSystemMessageText);
	// tvMessageText.setText(Farsi.Convert(chatMessage.getMsg()));
	// tvMessageText.setTypeface(typeface);
	// return systemMessageItemMainLayout;
	// }

	// private View operatorMessageSenderCase(ViewHolder holder)
	// {
	// View view;
	// view = inflater.inflate(R.layout.chat_user_message_item_right, null);
	// ViewGroup viewGroup = (ViewGroup) view
	// .findViewById(R.id.message_item_right_main_layout);
	// setTypeface(viewGroup);
	// holder.tvMessageText = (TextView) view
	// .findViewById(R.id.tvMessageTextRight);
	// // holder.tvMessageText
	// // .setMovementMethod(LinkMovementMethod.getInstance());
	// Settings currentSettings = BubblesManager.getInstance()
	// .getCurrentSettings();
	// if (currentSettings == null)
	// currentSettings = BubblesManager.getInstance().loadSettingsData(
	// context);
	// int fontSize = Integer.valueOf(currentSettings.getFontSize());
	// holder.tvMessageText.setTextSize(fontSize);
	// holder.tvMessageDate = (TextView) view
	// .findViewById(R.id.tvMessageDateRight);
	// holder.ivAvatar = (ImageView) view
	// .findViewById(R.id.ivMessageAvatarRight);
	// return view;
	// }

	// private void setTypeface(ViewGroup viewGroup)
	// {
	// UIManager.getInstance().setTypeface(context, viewGroup);
	// }

	// private View customerMessageSenderCase(ViewHolder holder)
	// {
	// View view;
	// view = inflater.inflate(R.layout.chat_user_message_item_left, null);
	// ViewGroup viewGroup = (ViewGroup) view
	// .findViewById(R.id.message_item_left_main_layout);
	// setTypeface(viewGroup);
	// holder.tvMessageText = (TextView) view
	// .findViewById(R.id.tvMessageTextLeft);
	// // holder.tvMessageText
	// // .setMovementMethod(LinkMovementMethod.getInstance());
	// Settings currentSettings = BubblesManager.getInstance()
	// .getCurrentSettings();
	// if (currentSettings == null)
	// currentSettings = BubblesManager.getInstance().loadSettingsData(
	// context);
	// int fontSize = Integer.valueOf(currentSettings.getFontSize());
	// holder.tvMessageText.setTextSize(fontSize);
	// holder.tvMessageDate = (TextView) view
	// .findViewById(R.id.tvMessageDateLeft);
	// holder.ivAvatar = (SmartImageView) view
	// .findViewById(R.id.ivMessageAvatarLeft);
	// holder.llMessageSent = (LinearLayout) view
	// .findViewById(R.id.llMessageSent);
	// return view;
	// }

	class ViewHolder {
		public TextView tvMessageText = null;
		public TextView tvMessageDate = null;

		public LinearLayout llMessageSent = null;
	}
}
