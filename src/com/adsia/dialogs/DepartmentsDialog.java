package com.adsia.dialogs;

import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RadioButton;

import com.adsia.adapter.DepartmentsAdapter;
import com.adsia.exampleApp.R;
import com.adsia.models.Department;

public class DepartmentsDialog extends Dialog implements
		android.view.View.OnClickListener, OnCheckedChangeListener {

	private Button btnDialogOk;
	private Button btneDialogCancel;

	private List<Department> departments;
	private ListView lvDepartments;
	private String selectedDepartmentId;
	private Department department;
	private OnDeptChangeListener listener;

	public DepartmentsDialog(Context context, List<Department> departments,
			OnDeptChangeListener listener) {
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		setContentView(R.layout.department_dialog);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		this.departments = departments;
		this.listener = listener;
		init(context);

	}

	private void init(Context context) {

		btnDialogOk = (Button) findViewById(R.id.btnDialogOK);
		btneDialogCancel = (Button) findViewById(R.id.btnDialogCancel);

		btnDialogOk.setOnClickListener(this);
		btneDialogCancel.setOnClickListener(this);
		
		lvDepartments = (ListView) findViewById(R.id.lvDepartments);

		// department adapter
		DepartmentsAdapter departmentsAdapter = new DepartmentsAdapter(this,
				departments);

		lvDepartments.setAdapter(departmentsAdapter);

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			ListView lv = (ListView) buttonView.getParent();
			int position = (Integer) buttonView.getTag();
			department = departments.get(position);
			for (int i = 0; i < lv.getChildCount(); i++) {
				RadioButton rb = (RadioButton) lv.getChildAt(i);
				rb.setChecked(false);
			}
			buttonView.setChecked(isChecked);
		}

	}

	@Override
	public void onClick(View view) {

		int id = view.getId();
		switch (id) {
		case R.id.btnDialogOK:
			dismiss();
			if (listener != null) {
				listener.onDialogConfirm(this, department);
			}

			break;
		case R.id.btnDialogCancel:
			dismiss();
			if (listener != null) {
				listener.onDiaogCancel(this);
			}
			break;
		}

	}

	@Override
	public void onBackPressed() {
		if (listener != null) {
			listener.onDiaogCancel(this);
		}
		super.onBackPressed();
	}

	/**
	 * Dialog Event Listener
	 */

	public interface OnDeptChangeListener {

		void onDialogConfirm(DepartmentsDialog DepartmentsDialog,
				Department department);

		void onDiaogCancel(DepartmentsDialog DepartmentsDialog);

	}

}
