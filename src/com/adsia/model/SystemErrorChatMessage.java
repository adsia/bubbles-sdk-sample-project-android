package com.adsia.model;

import com.adsia.models.ChatMessage;

public class SystemErrorChatMessage extends ChatMessage {
	public SystemErrorChatMessage() {
		super();
		setWho(SYSTEM_ERROR);
	}
}