package com.adsia.exampleApp;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

public class UIManager {

	private static UIManager instance = null;
	private ProgressDialog pd;

	private static Handler handler;

	public static UIManager getInstance() {
		if (instance == null)
			instance = new UIManager();
		return instance;
	}

	private UIManager() {
		handler = new Handler();
	}

	public void showToast(final Context context, final int msgId) {
		if (context == null)
			return;
		handler.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(context, "no message", Toast.LENGTH_LONG).show();

			}
		});
	}

	public static void showToast(final Context context, final String msg) {
		if (context == null)
			return;
		handler.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

			}
		});
	}

	public static void showToastWaitMessage(final Context context,
			final String msg) {
		if (context == null)
			return;
		handler.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
		});
	}

	private String getDate() {
		String messageDate = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
		messageDate += sdf.format(calendar.getTime());
		return messageDate;
	}

	public void showProgressDialog(final Context context, final int titleId,
			final int msgId) {
		handler.post(new Runnable() {

			@Override
			public void run() {
				try {
					if (context instanceof Activity) {
						Activity activity = (Activity) context;
						dismissProgressImmediately();
						if (!activity.isFinishing()) {
							pd = new ProgressDialog(context);
							pd.setCanceledOnTouchOutside(false);
							pd.setCancelable(false);
							// pd.setContentView(R.layout.progress_dialog);
							pd.show();

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	public void showProgressDialog(final Context context) {
		handler.post(new Runnable() {

			@Override
			public void run() {
				try {
					if (context instanceof Activity) {
						Activity activity = (Activity) context;
						dismissProgressImmediately();
						if (!activity.isFinishing()) {
							pd = new ProgressDialog(context);
							pd.setCanceledOnTouchOutside(false);
							pd.setCancelable(false);
							pd.show();
							// pd.setContentView(R.layout.progress_dialog);

						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	public void dismissProgressDialog() {
		handler.post(new Runnable() {
			@Override
			public void run() {
				dismissProgressImmediately();
			}
		});
	}

	private synchronized void dismissProgressImmediately() {
		if (pd != null && pd.isShowing()) {
			pd.dismiss();
		}
	}

}
