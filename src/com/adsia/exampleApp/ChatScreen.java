package com.adsia.exampleApp;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adsia.adapter.ChatMessagesAdapter;
import com.adsia.datamanagers.chat.ChatConnectionException;
import com.adsia.datamanagers.chat.ChatManager;
import com.adsia.datamanagers.chat.ChatManager.ChatListener;
import com.adsia.datamanagers.chat.ChatManager.ChatSessionListener;
import com.adsia.datamanagers.chat.ConfigManager;
import com.adsia.model.SystemErrorChatMessage;
import com.adsia.models.ChatMessage;
import com.adsia.models.WaitingData;

public class ChatScreen extends ActivityThatCanOpenChat implements
		ChatListener, ChatSessionListener {

	private ChatManager chatManager;
	private ChatMessagesAdapter chatMessagesAdapter;
	private ArrayList<ChatMessage> chatMessages = new ArrayList<ChatMessage>();

	protected ListView chatMessagesList;

	protected EditText chatMessageInputEditText;

	private Button sendMessageButton;

	private TextView isTypingTextView;

	private TextView tvOrderWord;
	private TextView tvOrder;
	private TextView tvOperatorText;
	private TextView tvExpectedTime;

	protected LinearLayout waitindDataContainer;

	@Override
	protected void onCreate(Bundle savedInstance) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstance);
		setContentView(R.layout.chat_screen);

		chatManager = ChatManager.getInstance(this.getApplicationContext());
		startListenOnChatEvents();

		chatMessagesList = (ListView) findViewById(R.id.lvChatMessages);
		chatMessageInputEditText = (EditText) findViewById(R.id.etUserMessage);

		isTypingTextView = (TextView) findViewById(R.id.tvTypingIndicator);

		chatMessagesAdapter = new ChatMessagesAdapter(this, chatMessages);
		chatMessagesList.setAdapter(chatMessagesAdapter);

		sendMessageButton = (Button) findViewById(R.id.btnSubmit);

		tvOrderWord = (TextView) findViewById(R.id.tvOrderWord);
		tvOrder = (TextView) findViewById(R.id.tvOrder);
		tvOperatorText = (TextView) findViewById(R.id.tvOperatorText);
		tvExpectedTime = (TextView) findViewById(R.id.tvExpectedTime);

		waitindDataContainer = (LinearLayout) findViewById(R.id.llWaitingData);

		if (chatManager.isServerBusy()) {
			disableSendingChatMessage();
			// showWaitingMessage(chatManager.getWaitingData().getWaitingMsg());
			waitingQueueUpdated(chatManager, chatManager.getWaitingData());
		} else {
			if (chatManager.isOperatorConnectedToChat()
					&& chatManager.isConnected()) {
				// if for some reason an operator joined
				enableSendingChatMessage();
			}
		}

	}

	private void disableSendingChatMessage() {
		sendMessageButton.setEnabled(false);
		chatMessageInputEditText.setEnabled(false);

		hideIsTyping();

	}

	private void hideIsTyping() {
		isTypingTextView.setText("");
		isTypingTextView.setVisibility(View.GONE);
	}

	private void showIsTyping() {
		isTypingTextView.setText("typing...");
		isTypingTextView.setVisibility(View.VISIBLE);
	}

	protected void enableSendingChatMessage() {
		sendMessageButton.setEnabled(true);
		chatMessageInputEditText.setEnabled(true);

	}

	private void startListenOnChatEvents() {
		chatManager.setChatListener(this);
		chatManager.setSessionListener(this);

	}

	@Override
	public void onMessageReceived(ChatManager chatManager,
			ChatMessage chatMessage) {
		if (chatMessage.getWho() == null)
			return;

		String operatorName = chatMessage.getNick();
		if (operatorName != null) {

			// chatMessage.setTimestamp(Common
			// .convertDateToMicroSecnods(new Date()) + "");
			appendAndScrollToChatMessage(chatMessage);
			enableSendingChatMessage();

		}
	}

	private void appendAndScrollToChatMessage(ChatMessage chatMessage) {
		chatMessages.add(chatMessage);
		chatMessagesAdapter.notifyDataSetChanged();

		// scrollChatToLast();
		scrollToLastView();

	}

	public void scrollToLastView() {
		chatMessagesList.setSelection(chatMessagesAdapter.getCount() - 1);
	}

	@Override
	public void onMessageFailedToDeliver(ChatManager chatManager,
			ChatMessage chatMessage) {
		try {
			chatMessage = (ChatMessage) chatMessage.clone();
			chatMessage.setWho(ChatMessage.FAILED_TO_BE_DELIVERED);
			appendAndScrollToChatMessage(chatMessage);
		} catch (CloneNotSupportedException e) {
			Log.e("ChatFragment", "Error while trying to clone a chat message",
					e);
		}
	}

	@Override
	public void onMessageDelivered(ChatManager chatManager,
			ChatMessage chatMessage) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOperatorLeftRoom(ChatManager chatManager) {
		cleanAllChatListeners();
		// chatManager.endSession();
		String bye_msg = null;
		if (bye_msg == null)
			bye_msg = "Good Bye...";
		showSystemErrorMessage(bye_msg);
		disableSendingChatMessage();

	}

	private void showSystemErrorMessage(String errorMessage) {
		if (errorMessage == null || errorMessage.length() == 0) {
			return;
		}
		removePreviousSystemMessages();
		SystemErrorChatMessage message = new SystemErrorChatMessage();
		message.setMsg(errorMessage);
		appendAndScrollToChatMessage(message);
	}

	protected void removePreviousSystemMessages() {

		ArrayList<ChatMessage> tmp = new ArrayList<ChatMessage>();
		tmp.addAll(chatMessages);
		for (ChatMessage msg : tmp) {
			if (msg.getWho() != null) {
				if (msg.getWho().equals(ChatMessage.SYSTEM_ERROR)) {
					chatMessages.remove(msg);
				}
			}
		}
	}

	private void cleanAllChatListeners() {
		chatManager.setChatListener(null);
		chatManager.setSessionListener(null);

	}

	protected boolean messagesContainsErrorMessage(String str) {
		boolean contains = false;
		if (str == null || str.length() == 0) {
			return contains;
		}
		for (ChatMessage m : chatMessages) {
			if (m.getWho() != null
					&& m.getWho().equals(ChatMessage.SYSTEM_ERROR)) {
				if (str.equals(m.getMsg())) {
					contains = true;
					break;
				}

			}
		}
		return contains;
	}

	@Override
	public void waitingQueueUpdated(ChatManager chatManager,
			WaitingData waitingData) {

		showWaitingMessage(waitingData.getWaitingMsg());

		waitindDataContainer.setVisibility(View.VISIBLE);
		tvOrder.setText(waitingData.getCustomersInFront() + "");
		String waitingTime = "Expected time :" + waitingData.getExpectedTime()
				+ "min(s)";
		tvExpectedTime.setText(waitingTime);

	}

	private void showWaitingMessage(String waitingMessage) {
		// will get it form callback

		if (waitingMessage == null) {
			return;
		}
		// chatManager.setWaitingMessage(null);
		showSuccessMessage(waitingMessage);

	}

	@Override
	public void onChatSessionOpened(ChatManager chatManager,
			String welcomeMessage) {
		showSuccessMessage(welcomeMessage);
	}

	@Override
	public void onChatSessionFailedToJoinRoom(ChatManager chatManager,
			String failedMessage) {
		showSystemErrorMessage(failedMessage);
		disableSendingChatMessage();

	}

	@Override
	public void onChatSessionRejoined(ChatManager chatManager) {
		showSuccessMessage("Re connected");
		enableSendingChatMessage();

	}

	public void showSuccessMessage(String message) {
		super.showSuccessMessage(message);
		// Log.d("TAG", message);
	}

	@Override
	public void onOperatorJoined(ChatManager chatManager,
			String operatorJoinedMessage, double joinTime, String sessionId) {
		hideWaitingView();
		showSuccessMessage(operatorJoinedMessage);
		enableSendingChatMessage();

	}

	private void hideWaitingView() {
		waitindDataContainer.setVisibility(View.GONE);

	}

	@Override
	public void onOperatorIsTyping(ChatManager chatManager) {

		showIsTyping();

	}

	@Override
	public void onOperatorReconnected(ChatManager chatManager) {
		showSuccessMessage("operator reconnected");
		enableSendingChatMessage();

	}

	@Override
	public void onOperatorDisconnected(ChatManager chatManager) {
		showSystemErrorMessage("operator disconnected");
		disableSendingChatMessage();

	}

	@Override
	public void onOperatorStoppedTyping(ChatManager chatManager) {
		hideIsTyping();

	}

	@Override
	public void onChatConnectionFailure(ChatManager chatManager,
			ChatConnectionException e) {
		// showSystemErrorMessage(getString(R.string.connection_error));
		showSystemErrorMessage(e.getErrorMessageString());
		disableSendingChatMessage();

	}

	@Override
	public void onOperatorTransferSession() {

		disableSendingChatMessage();
		String transferSession_msg = null;
		if (transferSession_msg == null)
			transferSession_msg = "You have been transfered.";
		UIManager.showToast(this, transferSession_msg);

	}

	@Override
	public void onChatTimeOut(String timeOutChatMessage) {

		Log.d("chatmanager", "On Chat Time out  message : "
				+ timeOutChatMessage);
		showSystemErrorMessage(timeOutChatMessage);
		disableSendingChatMessage();
		cleanAllChatListeners();

		chatManager.customerLeftChat();
		// chatManager.endSession();

	}

	// send message

	public void onSendMessageClicked(View v) {
		String msg = chatMessageInputEditText.getText().toString();
		if (msg.length() == 0) {
			showErrorMessage("You must enter message");
			return;
		}
		chatMessageInputEditText.setText("");
		ChatMessage chatMessage = new ChatMessage();

		chatMessage.setMsg(msg);
		String avatar = null;
		String name = null;

		chatMessage.setNick(name);
		chatMessage.setAvatar(avatar);
		chatManager.sendChatMessage(chatMessage);
		appendAndScrollToChatMessage(chatMessage);

	}

	@Override
	public void onTimeoutWarning(String message) {
		showSystemErrorMessage(message);

	}

	@Override
	public void onBackPressed() {

		disableSendingChatMessage();
		cleanAllChatListeners();
		chatManager.customerLeftChat();
//		chatManager.cleanUp();

		showFeedbackActivity();

		// super.onBackPressed();
	}

	private void showFeedbackActivity() {
		startActivity(new Intent(this, FeedbackActivity.class));
		finish();

	}

}
