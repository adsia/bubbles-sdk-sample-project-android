package com.adsia.exampleApp;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.adsia.datamanagers.chat.ChatManager;
import com.adsia.models.UserModel;

public class BaseActivity extends Activity {

	protected static Handler handler = null;

	public void showSuccessMessage(String msg) {
		if (msg == null || msg.length() == 0) {
			return;
		}
		UIManager.showToast(BaseActivity.this, msg);

	}

	public void showErrorMessage(int errorMessageId) {
		try {
			String msg = getString(errorMessageId);
			showErrorMessage(msg);
		} catch (Exception e) {
			Log.e("BaseActivity", "Erorr fetching string with id "
					+ errorMessageId, e);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		// Thread.setDefaultUncaughtExceptionHandler(new
		// CustomUncaughtExceptionHandler());
		if (handler == null) {
			handler = new Handler(Looper.getMainLooper());
		}
		// just to make sure they at least initialized inside UI Thread
		ChatManager.getInstance(getApplicationContext());
		UIManager.getInstance();

	}

	public void showErrorMessageDialog(final String errorMessage) {
		if (errorMessage == null || errorMessage.length() == 0) {
			return;
		}
		UIManager.showToast(BaseActivity.this, errorMessage);

	}

	public void showErrorMessage(final String errorMessage) {
		if (errorMessage == null || errorMessage.length() == 0) {
			return;
		}
		UIManager.showToast(BaseActivity.this, errorMessage);

	}

	protected void onSessionConfigFailedToLoad(String errorMessage,
			boolean showLoader) {
		if (showLoader) {
			showErrorMessage(errorMessage);
		}

	}

	protected void onSessionConfigLoaded() {
		// UserManager userManager = UserManager
		// .getInstance(getApplicationContext());
		// if (!userManager.hasValidLoggedInUser()
		// && !userManager.isLoggedInAsGuest()) {
		// showLoginScreen();
		// }
	}

	public void showLoader() {
		showLoader(null);
	}

	public void showLoader(String loadingMessage) {

		UIManager.getInstance().showProgressDialog(BaseActivity.this);
	}

	public void hideLoader() {
		UIManager.getInstance().dismissProgressDialog();

	}

	// save user data

	public void saveUserLocally(UserModel user) {

		SharedPreferences preferences = getSharedPreferences("user_data",
				MODE_PRIVATE);
		Editor edit = preferences.edit();
		edit.putString("user", user.toJson());
		edit.commit();

	}

	public UserModel getUserLocally() {
		SharedPreferences preferences = getSharedPreferences("user_data",
				MODE_PRIVATE);

		String data = preferences.getString("user", null);
		if (data != null) {

			try {

				JSONObject jsonObject = new JSONObject(data);
				UserModel user = new UserModel(jsonObject);
				return user;

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}

		}
		return null;

	}

}
