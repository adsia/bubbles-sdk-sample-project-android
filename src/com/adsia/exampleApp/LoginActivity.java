package com.adsia.exampleApp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.adsia.datamanagers.chat.ConfigManager;
import com.adsia.datamanagers.chat.RequestException;
import com.adsia.datamanagers.chat.UserManager;
import com.adsia.models.UserModel;

public class LoginActivity extends BaseActivity implements OnClickListener {

	// server data
	private static final String SERVER_APP_KEY = "c67d1af85d03efb63bc0f5310144952506988ed7";

	private static final String SERVER_NAME = "testing";

	private Button btnLogin, btnRegister;
	private EditText edUserEmail, edUserPassword, edUserEmailSignup,
			edUserNameSignup, edUserPasswordSignup;
	private TextView tvForgetPassword, tvRegister, tvEnterAsGuest;

	@Override
	protected void onCreate(Bundle savedInstance) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstance);

		setContentView(R.layout.activity_login);

		// set server configuration
		ConfigManager.setServerAppKey(SERVER_APP_KEY);
		ConfigManager.setServerName(SERVER_NAME);

		// check user

		if (getUserLocally() != null) {
			startActivity(new Intent(this, MainActivity.class));
			finish();
		} else {

			initViews();

		}

	}

	private void initViews() {

		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(this);

		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnRegister.setOnClickListener(this);

		edUserEmail = (EditText) findViewById(R.id.edUserEmail);
		edUserPassword = (EditText) findViewById(R.id.edUserPassword);
		edUserEmailSignup = (EditText) findViewById(R.id.edUserEmailSignup);
		edUserNameSignup = (EditText) findViewById(R.id.edUserNameSignup);
		edUserPasswordSignup = (EditText) findViewById(R.id.edUserPasswordSignup);

		tvForgetPassword = (TextView) findViewById(R.id.tvForgetPassword);
		tvForgetPassword.setOnClickListener(this);

		tvRegister = (TextView) findViewById(R.id.tvRegister);
		tvRegister.setOnClickListener(this);

		tvEnterAsGuest = (TextView) findViewById(R.id.tvEnterAsGuest);
		tvEnterAsGuest.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		final int id = v.getId();
		switch (id) {

		case R.id.btnLogin:
			new LoginUser().execute();
			break;

		case R.id.tvRegister:

			// convert the view to registration

			findViewById(R.id.registrationLayout).setVisibility(View.VISIBLE);
			findViewById(R.id.loginLayout).setVisibility(View.GONE);

			break;

		case R.id.tvForgetPassword:

			startActivity(new Intent(this , ResetPassword.class));
			
			break;
		case R.id.tvEnterAsGuest:

			startActivity(new Intent(this, MainActivity.class));

			break;

		case R.id.btnRegister:
			// make registration operation
			new RegistrationUser().execute();
			break;

		}

	}

	@Override
	public void onBackPressed() {

		int visiablity = findViewById(R.id.registrationLayout).getVisibility();

		if (visiablity == View.GONE)
			super.onBackPressed();
		else
			findViewById(R.id.registrationLayout).setVisibility(View.GONE);
		findViewById(R.id.loginLayout).setVisibility(View.VISIBLE);

	}

	/**
	 * Login User Operation
	 * 
	 * @author Mohamed
	 *
	 */
	private class LoginUser extends AsyncTask<Void, Void, Void> {

		RequestException e = null;

		@Override
		protected void onPreExecute() {

			LoginActivity.this.showLoader();
		}

		@Override
		protected Void doInBackground(Void... params) {

			String email = edUserEmail.getText().toString();
			String password = edUserPassword.getText().toString();

			try {
				UserModel model = UserManager.getInstance(LoginActivity.this)
						.loginWithEmail(email, password);
				saveUserLocally(model);

			} catch (RequestException e) {
				// TODO Auto-generated catch block
				this.e = e;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			LoginActivity.this.hideLoader();
			if (this.e == null) {
				// no error
				startActivity(new Intent(LoginActivity.this, MainActivity.class));
				finish();

			} else {

				if (e.getErrorMessage() != null
						|| e.getErrorMessage().length() > 0) {
					LoginActivity.this.showErrorMessage(e.getErrorMessage());
				}
			}

		}
	}

	/**
	 * Registration User Operation
	 * 
	 * @author Mohamed
	 *
	 */
	private class RegistrationUser extends AsyncTask<Void, Void, Void> {

		RequestException e = null;

		@Override
		protected void onPreExecute() {

			LoginActivity.this.showLoader();
		}

		@Override
		protected Void doInBackground(Void... params) {

			String email = edUserEmailSignup.getText().toString();
			String password = edUserPasswordSignup.getText().toString();
			String name = edUserNameSignup.getText().toString();

			try {
				UserModel model = UserManager.getInstance(LoginActivity.this)
						.registerNewUser(email, password, name);
				saveUserLocally(model);

			} catch (RequestException e) {
				// TODO Auto-generated catch block
				this.e = e;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			LoginActivity.this.hideLoader();
			if (this.e == null) {
				// no error
				startActivity(new Intent(LoginActivity.this, MainActivity.class));
				finish();

			} else {

				if (e.getErrorMessage() != null
						|| e.getErrorMessage().length() > 0) {
					LoginActivity.this.showErrorMessage(e.getErrorMessage());

				}
			}

		}
	}

	
}
