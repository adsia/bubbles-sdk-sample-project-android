package com.adsia.exampleApp;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.adsia.datamanagers.chat.ChatConnectionException;
import com.adsia.datamanagers.chat.ChatManager;
import com.adsia.datamanagers.chat.ChatManager.ChatSessionListener;
import com.adsia.datamanagers.chat.RequestException;
import com.adsia.dialogs.DepartmentsDialog;
import com.adsia.dialogs.DepartmentsDialog.OnDeptChangeListener;
import com.adsia.models.Department;
import com.adsia.models.UserLocation;
import com.adsia.models.WaitingData;

public class ActivityThatCanOpenChat extends BaseActivity implements
		ChatSessionListener, OnDeptChangeListener {

	private String userName;

	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);

	}

	protected void showChatScreen() {
		Intent intent = new Intent(this, ChatScreen.class);
		startActivity(intent);
	}

	private void cleanChatListener() {
		ChatManager chatManager = ChatManager
				.getInstance(getApplicationContext());
		chatManager.setChatListener(null);

		chatManager.setSessionListener(null);
	}

	@Override
	public void waitingQueueUpdated(ChatManager chatManager,
			WaitingData waitingData) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChatSessionOpened(ChatManager chatManager,
			String welcomeMessage) {
		hideLoader();
		cleanChatListener();
		showChatScreen();
	}

	@Override
	public void onChatSessionFailedToJoinRoom(ChatManager chatManager,
			String errorMessage) {
		cleanChatListener();
		chatManager.setCacheManager(null);
		hideLoader();
		showErrorMessage(errorMessage);

	}

	@Override
	public void onChatSessionRejoined(ChatManager chatManager) {
		// TODO Auto-generated method stub

	}

	public void enterChatWithName(String guestName) {

		ChatManager chatManager = ChatManager
				.getInstance(getApplicationContext());

		chatManager.setSessionListener(this);

		try {

			// check user 
			
			String id = null ; 
			if (getUserLocally() != null){
				
				id = getUserLocally().getId() ;
			}
			
			// dummy user location
			UserLocation userLocation = new UserLocation();
			userLocation.setLat(30.0081607);
			userLocation.setLong(31.1919482);

			chatManager.enterChatWithCustomerName(guestName, id,
					userLocation, null);

		} catch (RequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			hideLoader();
			showErrorMessage(e.getErrorMessage());

		}

	}

	// preparing for starting chat

	protected void prepareToChat(String userName) {

		// showLoader();

		this.userName = userName;
		enterChatWithName(userName);
	}

	public void onChatTimeOut(String arg0) {
		// TODO Auto-generated method stub

	}

	public void onTimeoutWarning(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChatConnectionFailure(ChatManager chatManager,
			ChatConnectionException error) {
		hideLoader();
		cleanChatListener();
		chatManager.cleanUp();
		// showErrorMessage(R.string.error_happened_in_connection_try_again);
		showErrorMessage(error.getErrorMessageString());

	}

	@Override
	public void onChatDeptartmentMustSelect(ChatManager chatManager,
			ArrayList<Department> onlineDepartment) {

		DepartmentsDialog dialog = new DepartmentsDialog(this,
				onlineDepartment, this);
		dialog.show();

		// for (Department department : onlineDepartment) {
		//
		// Log.d("TAG", department.getName());
		// }
		//
		// Toast.makeText(this,
		// "Selected Department : " + onlineDepartment.get(0).getName(),
		// Toast.LENGTH_LONG).show();
		//
		// ChatManager.getInstance(this).setSelectedDepartment(
		// onlineDepartment.get(0));
		//
		// prepareToChat(this.userName);

	}

	@Override
	public void onOperatorDisconnected(ChatManager chatManager) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOperatorReconnected(ChatManager chatManager) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOperatorTransferSession() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMissingCustomFieldsFailed(ChatManager chatManager,
			String string, JSONObject response) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPublicCustomFieldNeedToAccess(ChatManager chatManager,
			JSONObject response) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRequestOptionalCustomField(String customFieldName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onRequestAllowAccessOptionalCustomFailed(
			String customAccessFieldName) {
		// TODO Auto-generated method stub

	}

	/**
	 * Department dialog event listener
	 * 
	 * @param DepartmentsDialog
	 * @param department
	 */

	@Override
	public void onDialogConfirm(DepartmentsDialog DepartmentsDialog,
			Department department) {

		// check department

		if (department == null) {

			Toast.makeText(this, "Please select department", Toast.LENGTH_SHORT)
					.show();
		} else {
			ChatManager chatManager = ChatManager.getInstance(this);
			chatManager.setSelectedDepartment(department);
			chatManager.setSessionListener(this);

			try {
				chatManager.enterChatWithCustomerName(userName, null, null,
						null);
			} catch (RequestException e) {

				if (e != null && e.getMessage() != null) {
					Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT)
							.show();
				}

			}
		}

	}

	@Override
	public void onDiaogCancel(DepartmentsDialog DepartmentsDialog) {
		cleanChatListener();

	}
}
