package com.adsia.exampleApp;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.adsia.datamanagers.chat.ChatManager;
import com.adsia.datamanagers.chat.RequestException;
import com.adsia.datamanagers.chat.RequestResult;

public class FeedbackActivity extends BaseActivity implements OnClickListener {

	private RatingBar rbChatRate;
	private EditText etChatComment;
	private Button btnFeedbackSubmit;

	@Override
	protected void onCreate(Bundle savedInstance) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstance);

		setContentView(R.layout.feedback_activity);

		// initialize views
		rbChatRate = (RatingBar) findViewById(R.id.rbChatRate);
		etChatComment = (EditText) findViewById(R.id.etChatComment);

		btnFeedbackSubmit = (Button) findViewById(R.id.btnFeedbackSubmit);
		btnFeedbackSubmit.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		sendFeedback();
	}

	private void sendFeedback() {

		new Thread(new Runnable() {

			@Override
			public void run() {
				// send feedback

				String comment = etChatComment.getText().toString();
				int rate = (int) rbChatRate.getRating();
				ChatManager chatManager = ChatManager
						.getInstance(getApplicationContext());

				try {
					RequestResult result = chatManager.sendFeedback(rate,
							comment);
					if (result != null) {
						if (result.getJsonResponse() != null) {
							String msg = result.getJsonResponse().optString(
									"msg");

							chatManager.cleanUp();
							if (msg != null && msg.length() > 0) {
								showSuccessMessage(msg);
								finish();
							}
						}
					}
				} catch (RequestException e) {

				} finally {
					chatManager.cleanUp();
					// showSuccessMessage(getString(R.string.re));
				}

			}
		}).start();
	}

}
