package com.adsia.exampleApp;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.adsia.datamanagers.chat.ConfigManager;
import com.adsia.models.UserModel;

public class MainActivity extends ActivityThatCanOpenChat implements
		OnClickListener {

	// server data
	private static final String SERVER_APP_KEY = "c67d1af85d03efb63bc0f5310144952506988ed7";

	private static final String SERVER_NAME = "testing";

	// views

	private Button btnEnterChat;
	private EditText edUserName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		// set server configuration
		ConfigManager.setServerAppKey(SERVER_APP_KEY);
		ConfigManager.setServerName(SERVER_NAME);

		btnEnterChat = (Button) findViewById(R.id.btnEnterChat);
		btnEnterChat.setOnClickListener(this);
		edUserName = (EditText) findViewById(R.id.edUserName);

		UserModel user = getUserLocally();
		if (user != null) {
			edUserName.setText(user.getName());
			edUserName.setEnabled(false);
		}

	}

	@Override
	public void onClick(View v) {

		// enter chat event

		String userName = edUserName.getText().toString();
		if (userName == null || userName.length() == 0) {

			Toast.makeText(this, "Please Enter Username", Toast.LENGTH_LONG)
					.show();
		} else {

			// enter chat
			super.prepareToChat(userName);

		}

	}

}
