package com.adsia.exampleApp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.adsia.datamanagers.chat.RequestException;
import com.adsia.datamanagers.chat.UserManager;

public class ResetPassword extends BaseActivity implements OnClickListener {

	private EditText edUserResetEmail;
	private Button btnResetPassword;

	@Override
	protected void onCreate(Bundle savedInstance) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstance);
		setContentView(R.layout.activity_reset_pass);

		edUserResetEmail = (EditText) findViewById(R.id.edUserResetEmail);

		btnResetPassword = (Button) findViewById(R.id.btnResetPassword);
		btnResetPassword.setOnClickListener(this);

	}

	/**
	 * Reset User Password Operation
	 * 
	 * @author Mohamed
	 *
	 */
	private class ResetPasswordOperation extends AsyncTask<Void, Void, Void> {

		RequestException e = null;
		String message = null;

		@Override
		protected void onPreExecute() {

			ResetPassword.this.showLoader();
		}

		@Override
		protected Void doInBackground(Void... params) {

			String email = edUserResetEmail.getText().toString();

			try {
				message = UserManager.getInstance(ResetPassword.this)
						.resetPassword(email);

			} catch (RequestException e) {
				// TODO Auto-generated catch block
				this.e = e;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			ResetPassword.this.hideLoader();
			if (this.e == null) {
				// no error
				if (message != null)
					ResetPassword.this.showSuccessMessage(message);

			} else {

				if (e.getErrorMessage() != null
						|| e.getErrorMessage().length() > 0) {
					ResetPassword.this.showErrorMessage(e.getErrorMessage());

				}
			}

		}
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		switch (id) {
		case R.id.btnResetPassword:

			if (edUserResetEmail.getText().toString().length() > 0) {

				new ResetPasswordOperation().execute();
			} else {

				showErrorMessage("Please Enter valid Email");
			}

			break;

		default:
			break;
		}
	}

}
